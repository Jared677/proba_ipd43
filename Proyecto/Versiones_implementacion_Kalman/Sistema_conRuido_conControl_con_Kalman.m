clc; clear all; close all;

%%   Sistema
% x_k+1= Ax_k + Bu_k + v_k
%   y_k= Cx_k + Du_k + e_k
%

%% modelo motor discreto
h=0.1;
a=2.6667;
K=61.34;

A=[exp(-a*h) 0;(1-exp(-a*h))/a 1]; 
B=[1-exp(-a*h);exp(-a*h)/a-1/a+h]*K/a;
C=[0 1];
D=1;
E=[1;1];

%tiempo de simulacion
n=100;

%% Seteo de controlador
Omega=C'*C; %*0.00327;
Gamma=1;

N=5 %% k predicciones 
[H,Fa]=setmpc(A,B,N,Omega,Gamma)
LB=-1*ones(N,1);
UB=-LB;
G=[-eye(N);eye(N)];
g=[-LB;UB];


%% seguimiento ref

%Calculating the steady state shift to obtaing zero tracking error
Theta=[A-eye(2) B;0 1 0];
Thaux=Theta\[-B zeros(2,1);0 1]


%% ruido de salida
Pe=[3];
ue=0;
%e=zeros(1,ne);
e=sqrt(Pe)*rand(1,n)+ue;

%% ruido de entrada
Pv=[4];
uv=[0];
%v=zeros(2,ne);
v=sqrt(Pv)*rand(1,n)+uv;

%% condiciones iniciales
Xem1=[1;3]; %media inicial sistema
Pkm1=[0.2 0;0 0.2]; %varianza inicial sistema

Xp_=[0;0]; 
Pp_=[0 0;0 0];
x=sqrt(Pkm1)*rand(2,1)+Xem1; %condicion Inicial

u=0; %entrada inicial
r=50; %referencia

%% Simulacion de sistema y filtro
for k=1:n  %k=1 es estado inicial
    
%simulacion de sistema
     x(:,k+1)=A*x(:,k)+B*u(k)+E*v(:,k);     %estados
     y(k)=C*x(:,k)+D*e(k);         %salida

    %% kalman 
%Actualización de tiempo (prediccion)
    if k==1 %%instante uno la prediccion es c.i.
        Xp=Xp_;
        Pp=Pp_;
    else
        Xp=A*Xp_+B*u(k);
        Pp=A*Pp_*A'+Pv;
    end
%Actualizacion mediciones (correccion)
    Kk=Pp*C'/(C*Pp*C'+Pe);
    Xc=Xp+Kk*(y(k)-C*Xp);
    Pc=(eye(2)-Kk*C)*Pp;
%Generacion valores inicial para sig paso
    Xp_=Xc;
    Pp_=Pc;
    Xest(:,k)=Xc;

 %% Control LQR

%para seguimiento de referencia
Fa2=Fa*[Xc(1);Xc(2)-r];

%calculo de entrada
p=admms(H,Fa2,G,5*g,N); %admm  %%5 entrada maxima
%p=quadprog(H,Fa2);   %generico matlab

u(k+1)=p(1);  %se usa solo el primer elem (horizonte movil)
%u(k+1)=r-p1*Xest(:,k);



%% toma de datos
    Xest(:,k)=Xc;
end

figure(1)
plot(y,'b')
hold on
title('Seguimiento de la referencia')
plot(C*Xest,'k--')
plot([1 length(y)],[r r],'r--')
legend('y','y estimado','referencia')
ylabel('Y')
xlabel('k')
hold off

stop=n;
figure(2)
plot(Xest(1,[1:stop]),'b')
hold on
title('X1 y X1 est')
plot(x(1,[1:stop]),'r--')
legend('X1est','X1 real')
ylabel('x1')
xlabel('k')
hold off

figure(3)
plot(Xest(2,[1:stop]),'b')
hold on
title('X2 y X2 est')
plot(x(2,[1:stop]),'r--')
legend('X2est','X2 real')
ylabel('x2')
xlabel('k')
hold off


figure(4)
plot(u,'b')
hold on
title('Entrada')
legend('entrada')
ylabel('u')
xlabel('k')
hold off

error_x1 = abs(mean((x(1,1:length(Xest))-Xest(1,:))/x(1,1:length(Xest))))
error_x2 = abs(mean((x(2,1:length(Xest))-Xest(2,:))/x(2,1:length(Xest))))
error_control = abs((y-r*ones(1,length(y)))/(r*ones(1,length(y))))


function [H,Fa]=setmpc(A,B,N,Omega,Gamma)
[L,Omega_N] = dlqr(A,B,Omega,Gamma)
n = length(A); 
dimA = size(A,1);
dimB = size(B,2);
Oa = zeros(dimA*N,dimB*N);
 Aa =zeros(dimA*N,dimA);
 
   for k= 1:N
        Oa(n*k-(n-1):n*k,1) =(A^(k-1))*B;
    end
    
    for j = 2:N
        for k = j:N
            Oa(n*k-(n-1):n*k,j)=Oa(n*(k-1)-(n-1):n*(k-1),j-1);
        end
    end
        
    for k = 1:N
        Aa(n*k-(n-1):n*k,:) = A^k;
    end
        
    H = 2*(kron(eye(N),Gamma)...
        +Oa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa);
%    F = ((2*x'*Aa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa)');
 H=(H+H')/2;   
    Fa = ((2*Aa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa)');
    
%    LB = -Delta*ones(N,1);
%    UB = Delta*ones(N,1);
end


function u  = admms(H,h,G,g,N)
%===================================================================
% Quadratic programing (QP) problem solution:
%                   min 0.5 x'*H*x + h'*x
%                   s.t     F*x  = f
%                           G*x <= g                     
%   x      : Optimal solution of qp problem
%   val    : Value of the cost function in tk optimal
%===================================================================
%G=[-eye(N);eye(N)];
%g=[-LB;UB];
    n=size(H,1);
    q=size(G,1);
    %p=size(F,1);
    
    x=1*ones(n,1); 
    z=0.5*ones(q,1);
    %tau=0.5*ones(q+p,1); 
    tau=0.5*ones(q,1); 
     
%     A=[G; F]; 
%     B=[eye(q);zeros(p,q)];
%     c=[g;f];
        
    A=[G]; 
    B=[eye(q)];
    c=[g];
    
    ep=1e-9;  ed=1e-9; 
    %ep=eps;  ed=eps; 
    
%    rho=rhof(A,H)
%rho=207.5;
%rho=0.0081;
rho=0.1;
%rho=5;
%rho=4;%%Se tiene que escoger este valor
    rk=A*x+B*z-c;
    sk=rho*A'*B*(z); k=0;
    while norm(rk)>=ep || norm(sk)>=ed
        k=k+1;
        x=-(H+rho*A'*A)\(h+rho*A'*(B*z+tau-c));
%        z=max(0,-G*x-tau(1:q)+g);
        z=max(0,-G*x-tau+g);
        tau=tau+A*x+B*z-c;
        %if k==1000; break; end
        if k==5000; break; end %%Se tiene que escoger este valor
    end 
    %val=0.5*x'*H*x+h'*x;
    %x=k;
    %u_vec=k*ones(size(x));
    u=x;
    end