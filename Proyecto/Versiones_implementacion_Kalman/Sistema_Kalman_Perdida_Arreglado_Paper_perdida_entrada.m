clc; clear all; close all;
rng(50)
%%   Sistema
% x_k+1= Ax_k + Bu_k + v_k
%   y_k= Cx_k + Du_k + e_k
%

%% modelo motor discreto
h=0.1;
a=2.6667;
K=61.34;

A=[exp(-a*h) 0;(1-exp(-a*h))/a 1]; 
B=[1-exp(-a*h);exp(-a*h)/a-1/a+h]*K/a;
C=[0 1];
D=1;
E=[1;1];

%tiempo de simulacion
n=100;

%% Seteo de controlador
Omega=C'*C; %*0.00327;
Gamma=1;

N=5; %% k predicciones 
[H,Fa]=setmpc(A,B,N,Omega,Gamma);
LB=-1*ones(N,1);
UB=-LB;
G=[-eye(N);eye(N)];
g=[-LB;UB];


%% ruido de salida
Pe=[3];
ue=0;
%e=zeros(1,ne);
e=sqrt(Pe)*rand(1,n)+ue;

%% ruido de entrada
Pv=[4];
uv=[0];
%v=zeros(2,ne);
v=sqrt(Pv)*rand(1,n)+uv;

%% Simulación perdida de dato

%parametros
ra = 1;
rp = 0.8; %cambia % error

%variable bernoulli
rb=binornd(ra,rp,1,n); %para kalman
ru=binornd(ra,rp,1,n); %para entrada

%% condiciones iniciales
Xem1=[1;3]; %media inicial sistema
Pkm1=[0.2 0;0 0.2]; %varianza inicial sistema

Xp_=[0;0]; 
Pp_=[0 0;0 0];
x=sqrt(Pkm1)*rand(2,1)+Xem1; %condicion Inicial

u=0; %entrada inicial
r=50; %referencia

%% Simulacion de sistema y filtro
for k=1:n  %k=1 es estado inicial
    
%simulacion de sistema
     x(:,k+1)=A*x(:,k)+B*u(k)+E*v(:,k);     %estados
     y(k)=C*x(:,k)+D*e(k);         %salida

    %% kalman 

   yr(k)=rb(k)*y(k) + D*e(k);
%Actualización de tiempo (prediccion)
    if k==1 %%instante uno la prediccion es c.i.
        Xp=Xp_;
        Pp=Pp_;
    else
        Xp=A*Xp_+B*ru(k)*u(k);
        Pp=A*Pp_*A'+Pv;
    end
%Actualizacion mediciones (correccion)
    Kk=Pp*C'/(C*Pp*C'+Pe);
    Xc=Xp+rb(k)*Kk*(yr(k)-C*Xp);
    Pc=Pp-rb(k)*Kk*C*Pp;
%Generacion valores inicial para sig paso
    Xp_=Xc;
    Pp_=Pc;
    Xest(:,k)=Xc;

 %% Control LQR

%para seguimiento de referencia
Fa2=Fa*[Xc(1);Xc(2)-r];

%calculo de entrada
p=admms(H,Fa2,G,100*g,N); %admm  %%5 entrada maxima
%p=quadprog(H,Fa2);   %generico matlab

uc=p(1);  %se usa solo el primer elem (horizonte movil)

%u(k+1)=ru(k)*uc;  %entrada con perdida
u(k+1)=(1-ru(k))*u(k)+ ru(k)*uc;  %entrada corregida


%% toma de datos
    Xest(:,k)=Xc;
end

k = 1;
for i = 1:100
    u(i) = u(i)*ru(i);
    if u(i) == 0
        array(k) = i;
        k = k + 1;
    end
end

k = 1;
for i = 1:100
    if y(i)*rb(i) == 0
        array_2(k) = i;
        k = k + 1;
    end
end


for i = 1:length(array_2)
    nuevo_e(i) = e(array_2(i));
end


figure(10)
hold on
stem(u)
stem(array,zeros(1,length(array)),'*')
title('Entrada u[k] con pérdida de datos')
legend('u[k]','pérdida de dato')
ylabel('u[k]')
xlabel('k')
hold off

figure(11)
hold on
stem(yr)
stem(array_2,zeros(1,length(array_2))+nuevo_e)
title('Entrada y[k] con pérdida de datos')
legend('y[k]','pérdida de dato')
ylabel('y[k]')
xlabel('k')
hold off

figure(1)
plot(y,'b')
hold on

plot(C*Xest,'k--')
plot(r*ones(1,n))
title('Seguimiento de la referencia')
legend('y','y estimado','referencia')
ylabel('Y')
xlabel('k')
hold off

figure(5)
hold on
title('y salida')
plot(y,'c*')
stem(yr,'o')
legend('y salida real','y entrada kalman')
ylabel('Y [-]')
xlabel('k [muestras]')

stop=n;
figure(2)
plot(Xest(1,[1:stop]),'b')
hold on
title('X1 y X1 est')
plot(x(1,[1:stop]),'r--')
legend('X1est','X1 real')
ylabel('x1')
xlabel('k')
hold off

figure(3)
plot(Xest(2,[1:stop]),'b')
hold on
title('X2 y X2 est')
plot(x(2,[1:stop]),'r--')
legend('X2est','X2 real')
ylabel('x2')
xlabel('k')
hold off


figure(4)
plot(u,'b')
hold on
plot(ru,'*')
title('Entrada')
legend('entrada')
legend('entrada','perdida en la entrada')
ylabel('u')
xlabel('k')
hold off



function [H,Fa]=setmpc(A,B,N,Omega,Gamma)
[L,Omega_N] = dlqr(A,B,Omega,Gamma);
n = length(A); 
dimA = size(A,1);
dimB = size(B,2);
Oa = zeros(dimA*N,dimB*N);
 Aa =zeros(dimA*N,dimA);
 
   for k= 1:N
        Oa(n*k-(n-1):n*k,1) =(A^(k-1))*B;
    end
    
    for j = 2:N
        for k = j:N
            Oa(n*k-(n-1):n*k,j)=Oa(n*(k-1)-(n-1):n*(k-1),j-1);
        end
    end
        
    for k = 1:N
        Aa(n*k-(n-1):n*k,:) = A^k;
    end
        
    H = 2*(kron(eye(N),Gamma)...
        +Oa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa);
%    F = ((2*x'*Aa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa)');
 H=(H+H')/2;   
    Fa = ((2*Aa'*blkdiag(kron(eye(N-1),Omega),Omega_N)*Oa)');
    
%    LB = -Delta*ones(N,1);
%    UB = Delta*ones(N,1);
end

function u  = admms(H,h,G,g,N)
%===================================================================
% Quadratic programing (QP) problem solution:
%                   min 0.5 x'*H*x + h'*x
%                   s.t     F*x  = f
%                           G*x <= g                     
%   x      : Optimal solution of qp problem
%   val    : Value of the cost function in tk optimal
%===================================================================
%G=[-eye(N);eye(N)];
%g=[-LB;UB];
    n=size(H,1);
    q=size(G,1);
    %p=size(F,1);
    
    x=1*ones(n,1); 
    z=0.5*ones(q,1);
    %tau=0.5*ones(q+p,1); 
    tau=0.5*ones(q,1); 
     
%     A=[G; F]; 
%     B=[eye(q);zeros(p,q)];
%     c=[g;f];
        
    A=[G]; 
    B=[eye(q)];
    c=[g];
    
    ep=1e-9;  ed=1e-9; 
    %ep=eps;  ed=eps; 
    
%    rho=rhof(A,H)
%rho=207.5;
%rho=0.0081;
rho=0.1;
%rho=5;
%rho=4;%%Se tiene que escoger este valor
    rk=A*x+B*z-c;
    sk=rho*A'*B*(z); k=0;
    while norm(rk)>=ep || norm(sk)>=ed
        k=k+1;
        x=-(H+rho*A'*A)\(h+rho*A'*(B*z+tau-c));
%        z=max(0,-G*x-tau(1:q)+g);
        z=max(0,-G*x-tau+g);
        tau=tau+A*x+B*z-c;
        %if k==1000; break; end
        if k==5000; break; end %%Se tiene que escoger este valor
    end 
    %val=0.5*x'*H*x+h'*x;
    %x=k;
    %u_vec=k*ones(size(x));
    u=x;
    end