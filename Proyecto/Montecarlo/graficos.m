close all;
clc;
%% Inicio de variables de error
error_y = zeros(100,pruebas);
mean_y = zeros(100,1);

error_x1 = zeros(100,pruebas);
mean_x2 = zeros(100,1);

error_x2 = zeros(100,pruebas);
mean_x1 = zeros(100,1);

%% Plots
figure('Renderer','painter','Position',[500,200,900 600])
hold on
for k = 0:pruebas-1
    for j = 1:100
        error_x1(j,k+1) = abs((Array_supremo(j,5*k+1)-Array_supremo(j,5*k+3))/Array_supremo(j,5*k+3));
    end
    plot(error_x1(:,k+1))
end
title('Error estimación X1')
ylabel('Error')
xlabel('muestra K')
hold off


figure('Renderer','painter','Position',[500,200,900 600])
hold on
for k = 0:pruebas-1
    for j = 1:100
        error_x2(j,k+1) = abs((Array_supremo(j,5*k+2)-Array_supremo(j,5*k+4))/Array_supremo(j,5*k+4));
    end
    plot(error_x2(:,k+1))
end
title('Error estimación X2')
ylabel('Error')
xlabel('Muestra K')
hold off


figure('Renderer','painter','Position',[500,200,900 600])
hold on
for k = 0:pruebas-1
    for j = 1:100
        error_y(j,k+1) = abs((Array_supremo(j,5*k+5)-r)/r);
    end
    plot(error_y(:,k+1))
end
title('Error seguimiento a la referencia')
ylabel('Error')
xlabel('Muestra K')
hold off


figure('Renderer','painter','Position',[500,200,900 600])
hold on
for k = 0:pruebas-1
    plot(Array_supremo(:,5*k+5))
end
title('Seguimiento a la referencia')
ylabel('Error')
xlabel('Muestra K')
hold off

for i = 1:pruebas
    mean_y = error_y(:,i) + mean_y;
end
mean_y = mean_y/pruebas;

figure('Renderer','painter','Position',[500,200,900 600])
plot(mean_y)
title('Señal promedio de la salida')
ylabel('Error')
xlabel('Muestra k')

for i = 1:pruebas
    mean_x1 = error_x1(:,i) + mean_x1;
end
mean_x1 = mean_x1/pruebas;

figure('Renderer','painter','Position',[500,200,900 600])
plot(mean_x1)
title('Señal promedio del estado X1')
ylabel('Error')
xlabel('Muestra k')

for i = 1:pruebas
    mean_x2 = error_x2(:,i) + mean_x2;
end
mean_x2 = mean_x2/pruebas;

figure('Renderer','painter','Position',[500,200,900 600])
plot(mean_x2)
title('Señal promedio del estado X2')
ylabel('Error')
xlabel('Muestra k')


Error_mediax1 = mean(mean_x1);
Varianza_mediax1 = var(mean_x1);

Error_mediax2 = mean(mean_x2);
Varianza_mediax2 = var(mean_x2);

Error_mediay = mean(mean_y);
Varianza_mediay = var(mean_y);

medias = [Error_mediax1,Error_mediax2,Error_mediay];
varianzas = [Varianza_mediax1, Varianza_mediax2, Varianza_mediay];
disp('     X1         X2         Y')   
disp(medias)
disp(varianzas)



