clear 
clc
close all
%% Matrices del sistema
A = [0.5 0; 0 1.1];
B = [3; 1];
C = [1 0.1];
D = 1;

%% Condiciones iniciales
% Estado
mu_o = [-1; 2];
P_o = [0.5 0; 0 0.1];
% Ruido
mu_d = 0;
P_d = 0.1;
%% Calculo momentos del estado x

mu_x(1,:) = mu_o;
for k = 2 : 11
    mu_x(k,:) = A*mu_x(k - 1, :)';
end    

%P_x(1:2,:) = A.*P_o.*A' + B.*P_d.*B';
P_x(1:2,:) = P_o;
for k = 2 : 11
    P_x(2*k - 1:2*k,:) = A*P_x(2*k - 3:2*k - 2, :)*A' + B*P_d*B';
end    

% disp(mu_x)
% disp(P_x)

%% Calculo momentos de la salida y
for k = 1 : 11
    mu_y(k) = C*mu_x(k,:)' + D*mu_d;
end    
i = 0;
for k = 1 :2: 21
    P_y(k - i) = C*P_x(k:k+1,:)*C' + D*P_d*D';
    i = i + 1;
end    

%disp(mu_y')
%disp(P_y')

%% Calculo funcion de densidad (PDF) "a mano"

n = 1; %dimension
fun0 = @(y)((2*pi)^(n/2).*sqrt(P_y(1)))^(-1).*exp(-0.5*(y - mu_y(1)).*P_y(1)^(-1).*(y - mu_y(1)));
fun1 = @(y)((2*pi)^(n/2).*sqrt(P_y(2)))^(-1).*exp(-0.5*(y - mu_y(2)).*P_y(2)^(-1).*(y - mu_y(2)));
fun10 = @(y)((2*pi)^(n/2).*sqrt(P_y(11)))^(-1).*exp(-0.5*(y - mu_y(11)).*P_y(11)^(-1).*(y - mu_y(11)));
nro0 = integral(fun0,-1,1);
nro1 = integral(fun1,-1,1);
nro10 = integral(fun10,-1,1);

%% PDF para K = 0 con bibliotecas de MATLAB
y0 = linspace(-10,10,1000);
y0_region = linspace(-1,1,1000);
pdf0 = normpdf(y0,mu_y(1),P_y(1));
pdf0_region = normpdf(y0_region,mu_y(1),P_y(1));
Area_region0 = normcdf([-1,1], mu_y(1), sqrt(P_y(1)));
Total_no_sat0 = Area_region0(2) - Area_region0(1);

%Plots
figure('Renderer', 'painters', 'Position', [500,200,900 600])
subplot(2,1,1)
hold on
plot(y0,pdf0)
plot(y0_region,pdf0_region,'r-','LineWidth',1.5)
hold off
title('PDF k =0 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
subplot(2,1,2)
plot(y0_region,pdf0_region)
title('PDF k =0 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

%% PDF para K=1 con bibliotecas de MATLAB
y1 = linspace(-10,10,1000);
y1_region = linspace(-1,1,1000);
pdf1 = normpdf(y1,mu_y(2),P_y(2));
pdf1_region = normpdf(y1_region,mu_y(2),P_y(2));
Area_region1 = normcdf([-1,1], mu_y(2), sqrt(P_y(2)));
Total_no_sat1 = Area_region1(2) - Area_region1(1);

%Plots
figure('Renderer', 'painters', 'Position', [500,200,900 600])
subplot(2,1,1)
hold on
plot(y1,pdf1)
plot(y1_region,pdf1_region,'r-','LineWidth',1.5)
hold off
title('PDF k = 1 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
subplot(2,1,2)
plot(y1_region,pdf1_region)
title('PDF k = 1 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

%% PDF para K=10 con bibliotecas de MATLAB
y10 = linspace(-10,10,1000);
y10_region = linspace(-1,1,1000);
pdf10 = normpdf(y1,mu_y(11),P_y(11));
pdf10_region = normpdf(y1_region,mu_y(11),P_y(11));
Area_region10 = normcdf([-1,1], mu_y(11), sqrt(P_y(11)));
Total_no_sat10 = Area_region10(2) - Area_region10(1);

%Plots
figure('Renderer', 'painters', 'Position', [500,200,900 600])
subplot(2,1,1)
hold on
plot(y10,pdf10)
plot(y10_region,pdf10_region,'r-','LineWidth',1.5)
hold off
title('PDF k = 10 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
subplot(2,1,2)
plot(y10_region,pdf10_region)
title('PDF k = 10 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

%% Prints

disp("-------------------------------------CASO K = 0-------------------------------------")
disp("METODO Manual:")
disp("Probabilidad de NO estar saturado: " + nro0)
disp("Probabilidad de estar saturado: " + (1 - nro0))
disp("METODO Bibliotecas de MATLAB:")
disp("Probabilidad de NO estar saturado: " + Total_no_sat0)
disp("Probabilidad de estar saturado: " + (1 - Total_no_sat0))
%disp("------------------------------------------------------------------------------------")

disp("-------------------------------------CASO K = 1-------------------------------------")
disp("METODO Manual:")
disp("Probabilidad de NO estar saturado: " + nro1)
disp("Probabilidad de estar saturado: " + (1 - nro1))
disp("METODO Bibliotecas de MATLAB:")
disp("Probabilidad de NO estar saturado: " + Total_no_sat1)
disp("Probabilidad de estar saturado: " + (1 - Total_no_sat1))
%disp("------------------------------------------------------------------------------------")

disp("-------------------------------------CASO K = 10-------------------------------------")
disp("METODO Manual:")
disp("Probabilidad de NO estar saturado: " + nro10)
disp("Probabilidad de estar saturado: " + (1 - nro10))
disp("METODO Bibliotecas de MATLAB:")
disp("Probabilidad de NO estar saturado: " + Total_no_sat10)
disp("Probabilidad de estar saturado: " + (1 - Total_no_sat10))
disp("-------------------------------------------------------------------------------------")

%% Plots

figure('Renderer', 'painters', 'Position', [500,200,900 600])
subplot(3,2,1)
hold on
plot(y0,pdf0)
plot(y0_region,pdf0_region,'r-','LineWidth',1.5)
hold off
title('PDF k = 0 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
subplot(3,2,2)
plot(y0_region,pdf0_region)
title('PDF k = 0 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

subplot(3,2,3)
hold on
plot(y1,pdf1)
plot(y1_region,pdf1_region,'r-','LineWidth',1.5)
hold off
title('PDF k = 1 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

subplot(3,2,4)
plot(y1_region,pdf1_region)
title('PDF k = 1 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

subplot(3,2,5)
hold on
plot(y10,pdf10)
plot(y10_region,pdf10_region,'r-','LineWidth',1.5)
hold off
title('PDF k = 10 en todo el espacio')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

subplot(3,2,6)
plot(y10_region,pdf10_region)
title('PDF k = 10 en la region ded interes [-1,1]')
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');

sgtitle('Todas las PDF') 





figure('Renderer', 'painters', 'Position', [500,200,900 600])
hold on
a0 = plot(y0,pdf0);
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
M0 = "PDF k = 0";
a1 = plot(y1,pdf1);
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
M1 = "PDF k = 1";
a10 = plot(y10,pdf10);
xlabel('y'); ylabel('Probability Density $f_{Y}(y)$','Interpreter','latex');
M2 = "PDF k = 10";
hold off
legend([a0,a1,a10], [M0, M1, M2]);
title("PDF para los diferentes K")






