%% Modelo
clear;close all; clc
% X(t) = A * X(t-1) + V(t) 
% y(t) = C * X(t)   + E(t)
StateDim = 2;                       % Dimension de los estados
ObsDim = 1;                         % Dimension del observador                     
A = [0.95  0;
      0   0.7];
C = [2 1];
D = 0.5;
N  = 1000;                          % Número de puntos
X  = zeros(StateDim,N);            % Estados
y  = zeros(ObsDim,N);              % Salida (Observador)
%% Generacion de ruido en la entrada
Var_PNoise = 1;                    % Varianza del ruido en la entrada
Mu_PNoise = 0;                     % Media del ruido en la entrada
Std_PNoise = sqrt(Var_PNoise)';    % Desviación estándar del ruido en la entrada
PNoise = Std_PNoise * randn(StateDim,N) + Mu_PNoise*ones(StateDim,N);    % Ruido Gaussiano
a = [1 -0.5];b = [0 0.1];
a2 = [1 -0.8];b2 = [0 0.5];
v1 = filter(b,a,PNoise(1,:));      % Filtrando
v2 = filter(b2,a2,PNoise(2,:));    % Filtrando
PNoise = [v1; v2];                 % Ruido coloreado modelado ruido en la entrada
Q = cov(PNoise');                  % Matriz de Covarianza del ruido en la entrada
%% Generación ruido en la medición
Var_ONoise = 0.5;                  % Varianza del ruido de medición
Mu_ONoise = 0;                     % Media del ruido de medición
Std_ONoise = sqrt(Var_ONoise)';    % Desviación estándar del ruido de medición
ONoise = Std_ONoise * randn(ObsDim,N) + Mu_ONoise*ones(ObsDim,N);    % Ruido Gaussiano
R = cov(ONoise');                  % Matriz de Covarianza del ruido de medición
%% Valores iniciales del sistema
X(:,1) = [0 0]';                   % Estado inicial de los estados
y(1) = C * X(:,1) + ONoise(:,1);   % Estado inicial de la salida (observador)
%% Simulación del sistema
for i = 2 : N
    X(:,i) = A * X(:,i-1) + PNoise(:,i);   % Estados
    y(:,i) = C * X(:,i)   + D*ONoise(:,i); % Salidas "reales"
end
%% Filtro de Kalman 
xh = zeros(StateDim,1);            % Estado inicial del valor actual
Px = [0.2 0; 0 0.2];               % Estado inicial de la matriz de covarianza de los estados
for i = 1 : N
    %---------- Prediccion ----------
    % Prediccion a priori del estado actual
    xh_(:,i) = A * xh(:,i) + PNoise(:,i);
   
    % Predicción a priori del estado actual de la matriz de covarianza de los estados
    Px_ = A*Px*A' + Q;
   
    %---------- Actualización (corrección) ----------
    % Coeficiente de Kalman
    K = Px_ * C' * inv(C*Px_*C' + R);
   
    % Estimiación de la salida (observador)
    yh_(:,i) = C * xh_(:,i) + R;
    
    % Cálculo de la innovación (diferencia entre salida real y estimada)
    inov(:,i) = y(:,i) - yh_(:,i);
    
    % Actualización (Corrección) de los estados
    xh(:,i+1) = A * xh_(:,i) + K * inov(:,i);
    
    % Actualización (Corrección) de la matriz de covarianza de los estados
    Px = Px_ - K*C*Px_;
   
end

%% Calculo de varianza
error_X1 = abs((xh(1,1:1000) - xh_(1,:))/xh(1,1:1000))
error_X2 = abs((xh(2,1:1000) - xh_(2,:))/xh(2,1:1000))
%% Graficando estimación de resultados 
figure('Renderer','painter','Position',[500,200,900 600])
plot(y,'b')
hold on, plot(yh_,'r')
grid on
legend('Observación real','Observación estimada') 
title('salida')
xlabel('muestras[-]');ylabel('Amplitud[-]')

figure('Renderer','painter','Position',[500,200,900 600])
plot(X(1,:),'b')
hold on, plot(xh(1,:),'r')
grid on
legend('Observación real','Observación estimada')
title('X1')
xlim([0 N]);xlabel('muestras[-]');ylabel('Amplitud[-]')

figure('Renderer','painter','Position',[500,200,900 600])
plot(X(2,:),'b')
hold on, plot(xh(2,:),'r')
grid on
legend('Observación real','Observación estimada')
title('X2')
xlim([0 N]);xlabel('muestras[-]');ylabel('Amplitud[-]')
