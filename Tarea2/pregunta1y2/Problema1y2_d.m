clear;
clc;
close all;

%% Inicialización de variables
load datos_Jared.mat
alpha = 0.05;
N = 1000;
NB = 20;
H = rand(4,1);
p = rand(4,1);
dist = ["Triangular","Uniforme","Normal","Exponencial"];
%% Distribuciones 
% Caso 1000 muestras
pd1 = makedist('Triangular',-1,0,1);
pd2 = makedist('Uniform',-1,2);
pd3 = makedist('Normal',mean(datos_Jared_a),sqrt(var(datos_Jared_a)));
pd4 = makedist('Exponential', mean(datos_Jared_a));
% Computando pdfs
x = linspace(-2,2,N);
pdf1 = pdf(pd1,x);
pdf2 = pdf(pd2,x);
pdf3 = pdf(pd3,x);
pdf4 = pdf(pd4,x);
% Caso 20 muestras
pd3B = makedist('Normal',mean(datos_Jared_b),sqrt(var(datos_Jared_b)));
%pd4B = makedist('Exponential', mean(datos_Jared_b)); No se puede puesto
%que la media del conjunto de datos en menor a cero.
% Computando pdfs
xB = linspace(-2,2,NB);
pdf1B = pdf(pd1,xB);
pdf2B = pdf(pd2,xB);
pdf3B = pdf(pd3B,xB);
%pdf4B = pdf(pd4B,xB);
%% Test de Hipotesis
% Caso 1000 muestras
[H(1),p(1)] = kstest(datos_Jared_a,pd1,alpha);
[H(2),p(2)] = kstest(datos_Jared_a,pd2,alpha);
[H(3),p(3)] = kstest(datos_Jared_a,pd3,alpha);
[H(4),p(4)] = kstest(datos_Jared_a,pd4,alpha);
disp('Caso 1000 muestras con test KS')
for i = 1:4
    if H(i) == true
        if p(i) < 0.0001 p(i) = 0; end
        disp("Se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    else
        disp("No se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    end
end

% Caso 20 muestras
disp('Caso 20 muestras con test Anderson-Darling')
[H(1),p(1)] = adtest(datos_Jared_b,'Distribution',pd1);
[H(2),p(2)] = adtest(datos_Jared_b,'Distribution',pd2);
[H(3),p(3)] = adtest(datos_Jared_b,'Distribution',pd3B);
for i = 1:3
    if H(i) == true
        if p(i) < 0.0001 p(i) = 0; end
        disp("Se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    else
        disp("No se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    end
end

disp('Caso 20 muestras con test KS')
[H(1),p(1)] = kstest(datos_Jared_b,pd1);
[H(2),p(2)] = kstest(datos_Jared_b,pd2);
[H(3),p(3)] = kstest(datos_Jared_b,pd3B);
for i = 1:3
    if H(i) == true
        if p(i) < 0.0001 p(i) = 0; end
        disp("Se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    else
        disp("No se rechaza la hipotesis nula para la distribución " + dist(i) + " , p = " + p(i))
    end
end
%% Graficando
% Comparación con la triangular
figure('Renderer','painter','Position',[500,200,900 600])
sgtitle("Comparación con distribucion triangular")
subplot(1,2,[1 2])
hold on
cdfplot(datos_Jared_a)
plot(x,cdf(pd1,x))
hold off
title("Función acumulada (1000 muestras)")
legend('Muestra de datos','Distribución')

figure('Renderer','painter','Position',[500,200,900 600])
sgtitle("Comparación con distribucion triangular")
subplot(1,2,[1 2])
hold on
cdfplot(datos_Jared_b)
plot(xB,cdf(pd1,xB))
hold off
title("Función acumulada (20 muestras)")
legend('Muestra de datos','Distribución')

% Comparación con la uniforme
figure('Renderer','painter','Position',[500,200,900 600])
sgtitle("Comparación con distribucion uniforme")
subplot(1,2,[1 2])
hold on
cdfplot(datos_Jared_a)
plot(x,cdf(pd2,x))
hold off
title("Función acumulada (1000 muestras)")
legend('Muestra de datos','Distribución')

figure('Renderer','painter','Position',[500,200,900 600])
subplot(1,2,[1 2])
sgtitle("Comparación con distribucion uniforme")
hold on
cdfplot(datos_Jared_b)
plot(xB,cdf(pd2,xB))
hold off
title("Función acumulada (20 muestras)")
legend('Muestra de datos','Distribución')

% Comparación con la Normal
figure('Renderer','painter','Position',[500,200,900 600])
sgtitle("Comparación con distribucion Normal")
subplot(1,2,[1 2])
hold on
cdfplot(datos_Jared_a)
plot(x,cdf(pd3,x))
hold off
title("Función acumulada (1000 muestras)")
legend('Muestra de datos','Distribución')

figure('Renderer','painter','Position',[500,200,900 600])
subplot(1,2,[1 2])
sgtitle("Comparación con distribucion Normal")
hold on
cdfplot(datos_Jared_b)
plot(xB,cdf(pd3B,xB))
hold off
title("Función acumulada (20 muestras)")
legend('Muestra de datos','Distribución')

% Comparación con la exponencial (Solo 1000 muestras)
figure('Renderer','painter','Position',[500,200,900 600])
subplot(1,2,[1 2])
sgtitle("Comparación con distribucion exponencial")
hold on
cdfplot(datos_Jared_a)
plot(x,cdf(pd4,x))
hold off
title("Función acumulada (1000 muestras)")
legend('Muestra de datos','Distribución')

figure('Renderer','painter','Position',[500,200,900 600])
hold on
histogram(datos_Jared_a, 100)
plot(x,12*ones(1,N),'Linewidth',3)
xlim([-1 2]);
hold off
title("Histograma 100 bins")
legend('Histograma','Distribución uniforme')

figure('Renderer','painter','Position',[500,200,900 600])
histogram(datos_Jared_b, 15)
