function test_blancura(datos,N,tau,k)

R = zeros(tau,1); % inicializacion de var de autocovarianza

%Estumacion de la autocovarianza
for j = 1 : k
    for i = 1 : N-tau
        R(j) = (datos(i+tau) - datos(i));
    end
end
R = R/N;

%Rechaza o no la hipotesis mediante metodo visto en clases
if sum(R.^2) > (k + 1.65*sqrt(2*k))*R(1).^2/N
    disp("Se rechaza la hipotesis nula para el caso de " + N + " muestras, " + sum(R.^2) + " > " + (k + 1.65*sqrt(2*1000))*R(1).^2/1000)
else
    disp("No se rechaza la hipotesis nula para el caso de " + N + " muestras")
end

end