clear;
clc;
close all;

%% Inicialización de variables
load datos_Jared.mat
media_muestral(1000) = (0);
varianza_muestral(1000) = (0);
media_muestralB(20) = (0);
varianza_muestralB(20) = (0);
t = linspace(1,1000,1000);
tB = linspace(1,20,20);

%% Cálculo de media y varianza
for i = 1 : 1000
    media_muestral(i) = mean(datos_Jared_a(1:i));
end

for i = 1 : 1000
    varianza_muestral(i) = var(datos_Jared_a(1:i));
end

for i = 1 : 20
    media_muestralB(i) = mean(datos_Jared_b(1:i));
end

for i = 1 : 20
    varianza_muestralB(i) = var(datos_Jared_b(1:i));
end

estabilidad_media = 0.53*ones(1,1000);
estabilidad_varianza = 0.73*ones(1,1000);

%% Graficando medias y varianzas
figure('Renderer','painter','Position',[500,200,900 600])
hold on
plot(t,media_muestral)
plot(t,estabilidad_media)
title('Evolución media muestral')
ylabel('Valor');xlabel('n');
ylim([0 1]);
legend('Evolución media muestral','0.53')
hold off

figure('Renderer','painter','Position',[500,200,900 600])
hold on
plot(t,varianza_muestral)
plot(t,estabilidad_varianza)
title('Evolución varianza muestral (1000 datos)')
ylabel('Valor');xlabel('n')
ylim([0 1]);
legend('Evolución varianza muestral (1000 datos)','0.73')
hold off

figure('Renderer','painter','Position',[500,200,900 600])
plot(tB,media_muestralB)
title('Evolución media muestral (20 datos)')
ylabel('Valor');xlabel('n');
ylim([-0.4 1.2]);xlim([0 20]);

figure('Renderer','painter','Position',[500,200,900 600])
plot(tB,varianza_muestralB)
title('Evolución varianza muestral (20 datos)')
ylabel('Valor');xlabel('n')
ylim([0 4]);xlim([0 20]);
