clear;
clc;
close all;

%% Inicialización de variables
load datos_Jared.mat

%% Espectrogramas
%Caso 1000 muestras
rect1000 = periodogram(datos_Jared_a,rectwin(length(datos_Jared_a))); 
blackman1000 = periodogram(datos_Jared_a,blackman(length(datos_Jared_a))); 
hamming1000 = periodogram(datos_Jared_a,hamming(length(datos_Jared_a))); 
%Caso 20 muestras
rect20 = periodogram(datos_Jared_b,rectwin(length(datos_Jared_b))); 
blackman20 = periodogram(datos_Jared_b,blackman(length(datos_Jared_b))); 
hamming20 = periodogram(datos_Jared_b,hamming(length(datos_Jared_b))); 

%% Test de blancura
% Caso 1000 muestras
tau = 1;
k = 1;
N = 1000;
datos = datos_Jared_a;
test_blancura(datos,N,tau,k)
% Caso 20 muestras
tau = 1;
k = 1;
N = 20;
datos = datos_Jared_b;
test_blancura(datos,N,tau,k)

%% Graficando 
figure('Renderer','painter','Position',[500,70,900 920])
subplot(3,2,[1 2])
sgtitle("PSD")
periodogram(datos_Jared_a,rectwin(length(datos_Jared_a))); 
title("Rect")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");
subplot(3,2,[3 4])
periodogram(datos_Jared_a,blackman(length(datos_Jared_a)));
title("Blackman")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");
subplot(3,2,[5 6])
periodogram(datos_Jared_a,hamming(length(datos_Jared_a)));
title("Hamming")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");

figure('Renderer','painter','Position',[500,70,900 920])
subplot(3,2,[1 2])
sgtitle("PSD")
periodogram(datos_Jared_b,rectwin(length(datos_Jared_b))); 
title("Rect")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");
subplot(3,2,[3 4])
periodogram(datos_Jared_b,blackman(length(datos_Jared_b)));
title("Blackman")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");
subplot(3,2,[5 6])
periodogram(datos_Jared_b,hamming(length(datos_Jared_b))); 
title("Hamming")
ylabel('Potencia/Frecuencia (dB/(rad/muestras))');xlabel("Frecuencia Normalizada (x pi rad/muestras)");









